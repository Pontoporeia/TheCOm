<?php
    include("librairies/Parsedown.php");
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $dir = $_GET['dir'];

    $Parsedown = new Parsedown();
    $chemin = "data/" .$dir. "/";

    $content = file_get_contents($chemin. 'content.md');
    $content = $Parsedown->text($content);

    $dirProjet = array();
    $MyDirectory = opendir('data') or die('Erreur');
    while($Entry = readdir($MyDirectory)) {
      if(is_dir($MyDirectory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
      } elseif ($Entry !='.' && $Entry != '..') {
        array_push($dirProjet,$Entry);
      }
    }
    closedir($MyDirectory);

    sort($dirProjet);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style/object.css">
    <title>Page des objets</title>
</head>
<body>

    <a href="index.php"><div class="logo">
        <img src="img/an_logo.png" alt="">
    </div></a>
    <div class="about_container">
        <div class="about"></div>
    </div>
    <div id="title">
        <p><?= $dir ?></p>
    </div>
    <div class="synopsis">
        <p><?= $content ?></p>
    </div>
    <div class="object">
        <img src="<?= $chemin.'home.jpg'?>" alt="">
    </div>
    <div class="allobject">
        <a href="object.php?dir=<?= $dirProjet[0] ?>"><div id="object1" class="gameobject"></div></a>
        <a href="object.php?dir=<?= $dirProjet[1] ?>"><div id="object2" class="gameobject"></div></a>
        <a href="object.php?dir=<?= $dirProjet[2] ?>"><div id="object3" class="gameobject"></div></a>
        <a href="object.php?dir=<?= $dirProjet[3] ?>"><div id="object4" class="gameobject"></div></a>
        <div id="object5" class="gameobject"></div>
        <div id="object6" class="gameobject"></div>
        <div id="object7" class="gameobject"></div>
    </div>

</body>
</html>
