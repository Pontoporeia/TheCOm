<?php
  include('librairies/Parsedown.php');

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);



  $dirProjet = array();
  $MyDirectory = opendir('data') or die('Erreur');
  while($Entry = readdir($MyDirectory)) {
    if(is_dir($MyDirectory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
    } elseif ($Entry !='.' && $Entry != '..') {
      array_push($dirProjet,$Entry);
    }
  }
  closedir($MyDirectory);

  sort($dirProjet);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style/main.css?version=51">
    <script src="https://unpkg.com/masonry-layout@4.2.2/dist/masonry.pkgd.min.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="logo">
      <img src="img/an_logo.png" alt="">
    </div>
    <div class="about_container">
        <div class="about" id="post_it"></div>
    </div>
    <?php

    ?>
    <div class="main_container">
        <div class="masonry">
            <div class="curiosity_object"></div>
            <a href="object.php?dir=<?= $dirProjet[0] ?>"><div id="object1" class="object"></div></a>
            <a href="object.php?dir=<?= $dirProjet[1] ?>"><div id="object2" class="object"></div></a>
            <a href="object.php?dir=<?= $dirProjet[2] ?>"><div id="object3" class="object object-width-2"></div></a>
            <a href="object.php?dir=<?= $dirProjet[3] ?>"><div id="object4" class="object object-height-2"></div></a>
            <div id="object5" class="object"></div>
            <div id="object6" class="object objetc-width-2"></div>
            <div id="object7" class="object object-height-2"></div>
        </div>  
    </div>
</body>
</html>