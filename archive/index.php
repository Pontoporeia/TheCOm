<?php
  include('librairies/Parsedown.php');

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);



  $dirProjet = array();
  $MyDirectory = opendir('data') or die('Erreur');
  while($Entry = readdir($MyDirectory)) {
    if(is_dir($MyDirectory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
    } elseif ($Entry !='.' && $Entry != '..') {
      array_push($dirProjet,$Entry);
    }
  }
  closedir($MyDirectory);

  sort($dirProjet);
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cabinet de curiosité</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style/main.css">
  </head>
  <body>
    
    <!-- logo de an site -->
    <a href="../index.php"> <img class="logo" src="img/an_logo.png" alt=""></a>


    <div id="title"><h1>Cabinet de Curiosités<br></h1></div>


    <?php
      $count=count($dirProjet);

      $Parsedown = new Parsedown();
      $images = array();
      for ($i = 0; $i < $count; $i++){
        $path = 'data/'. $dirProjet[$i] . '/';
        $MyDirectory = opendir($path) or die('Erreur');
        while($Entry = readdir($MyDirectory)) {
          (preg_match('#\.(md)$#i', $Entry)) ? $text = $Entry : '';
        }
        $content = file_get_contents($path . $text);
        $content = $Parsedown->text($content);

        preg_match( '/<h1>(.*?)<\/h1>/', $content, $title );
      ?>
        <section>
          <a id="pp" href="item.php?dir=<?= $dirProjet[$i] ?>">
            <img class="link_image" src="<?= $path ?>home.jpg" alt="">
            <h2><?= $title[0] ?></h2>
          </a>
        </section>
      <?php
      }
    ?>

<!-- les bouttons "about" et "index" -->

<div class="button" id="button1"><a href="">À propos<br><div class="ligne">________</div></a></div>
    <div class="button" id="button2"><a href="">Index<br><div class="ligne">________</div></a></div>


    <div id="slider">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  </body>
</html>
