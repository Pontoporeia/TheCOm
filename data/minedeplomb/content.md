*De l'inusable à l'obsolence programmée...saga d'un crayon.*



Nous sommes au Moyen-Age et contrairement aux idées reçues, nous assistons à un essor intellectuel et un rayonnement culturel remarquables.

Cette croissance est à l'origine de nombreux progrès ert notamment dans le domaine de l'écriture. C'est ainsi que la mine de plomb fait son apparition probablement vers le 11ème siècle. Cette découverte offre de nombreux avantages et est amplement utilisée au quotidien par les lettrés et copistes. En réalité, on emploie déja le plomb comme moyen d'écriture au 2ème siècle. En effet, les Romains se servaient alors d'un morceau de plomb en forme de plaque ronde pour tracer les lignes.

Revenons à notre mine de plomb médiévale. Contrairement à son ancêtre, sa forme est proche de notre crayon. Le cylindre se termine d'un côté par une pointe et de l'autre par un demi-cercle ce qui permet à la fois d'écrire et de faire des droites.

Le principal atout de cette mine est d'être inusable. Mais alors me direz-vous, pourquoi ne l'utilisons-nous plus actuellement ? Pire, quasi plus personne ne connait cet objet et son usage.



*La mine de plomb a t-elle été sacrifiée sur l'autel de l'obsolence programmée ?*



![La mine de plomb à ses origines.](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Mine_de_plomb_ancienne-h1.jpg/260px-Mine_de_plomb_ancienne-h1.jpg)

